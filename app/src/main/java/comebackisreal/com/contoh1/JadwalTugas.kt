package comebackisreal.com.contoh1

import android.content.Context
import android.os.Bundle
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.PeriodicTask

class JadwalTugas (var context: Context){
    private val myGcmNetworkManager =
        GcmNetworkManager.getInstance(context);

    fun createPeriodikTask(asalNegara : String?){
        var bundle = Bundle()
        bundle.putString("asalNegara",asalNegara)
        var myTask = PeriodicTask.Builder()
            .setService(GCMCuacaHariIni::class.java)
            .setPeriod(60L)
            .setFlex(10)
            .setTag(TagCuaca)
            .setExtras(bundle)
            .setPersisted(true)
            .build()
        myGcmNetworkManager.schedule(myTask)
    }

    fun cancelPeriodikTask(){
        if(myGcmNetworkManager!=null){
            myGcmNetworkManager.
                cancelTask(TagCuaca,GCMCuacaHariIni::class.java)
        }
    }
}

