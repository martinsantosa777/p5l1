package comebackisreal.com.contoh1

import android.content.Intent
import android.preference.PreferenceActivity
import android.util.Log
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.GcmTaskService
import com.google.android.gms.gcm.TaskParams
import com.loopj.android.http.AsyncHttpResponseHandler
import com.loopj.android.http.SyncHttpClient
import cz.msebera.android.httpclient.Header

const val TagCuaca ="TAG_CUACA"
class  GCMCuacaHariIni : GcmTaskService() {
    override fun onRunTask(p0: TaskParams?): Int {
        var result = 0
        if(p0!=null && p0.tag.equals(TagCuaca)){
            getCuacaHariIni(p0.extras.getString("asalNegara"))
            result = GcmNetworkManager.RESULT_SUCCESS
        }
        return result;
    }
    override fun onInitializeTasks() {
        super.onInitializeTasks()
        var myJadwalTugas = JadwalTugas(this)
        myJadwalTugas.createPeriodikTask("Indonesia")
    }
    fun getCuacaHariIni(asalNegara : String?) {
        /*
        val AppID = "cccdf75d45768caa8a9c5840e3319465"
        val Kota = "Medan"
        */

        val TAG = GCMCuacaHariIni::class.java.simpleName
        var client = SyncHttpClient()

        /*
        var url = "http://api.openweathermap.org/data/2.5/"+
                "weather?q=$Kota&APPID=$AppID"
        */

        var url = "https://uinames.com/api/"
        if(asalNegara!=null && asalNegara!="")
            url = url + "?region=" + asalNegara

        val charset = Charsets.UTF_8
        var handler = object : AsyncHttpResponseHandler(){
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, responseBody: ByteArray?) {
                var result : String="";
                if(responseBody!=null) {
                    result = responseBody.toString(charset)
                    Log.w("Json", result)
                }
                Log.d(TAG, result)

                //pengiriman broadcast
                var broadcastIntent = Intent()
                broadcastIntent.setAction("User data")
                broadcastIntent.putExtra("String JSON", result)
                sendBroadcast(broadcastIntent)
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?) {
                Log.d(TAG,"Faild")
            }
        }
        client.get(url,handler)
    }

}