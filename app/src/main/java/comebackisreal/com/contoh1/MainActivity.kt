package comebackisreal.com.contoh1

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.nio.channels.InterruptedByTimeoutException

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var mJadwal = JadwalTugas(this)

        startJob.setOnClickListener {
            mJadwal.createPeriodikTask(txtNegara.text.toString())
        }
        cancelJob.setOnClickListener {
            mJadwal.cancelPeriodikTask()
        }

        var GCMREceiver = object : BroadcastReceiver(){
            override fun onReceive(context: Context?, intent: Intent?) {
                var bundle = intent!!.getStringExtra("String JSON")
                txtHasilRandom.text = bundle
            }
        }

        var IntentFilter = IntentFilter("User data")
        registerReceiver(GCMREceiver, IntentFilter)

    }

}
